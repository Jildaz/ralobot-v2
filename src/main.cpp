#include <Arduino.h>
#include <AFMotor.h>
#include <FlexiTimer2.h>
#include <Servo.h>

//** MACRO CARACTRES **//
#define ASCII_NORECV	-1 // Aucune valeur dans le buffer serie
#define ASCII_LF		10 // Valeur ASCII du Line Feed
#define ASCII_0 		48 // Valeur ASCII du caractère 0
#define ASCII_9 		57 // Valeur ASCII du caractère 9
#define ASCII_V			86 // Valeur ASCII du caractère V

//** MACRO MOTEURS **//
#define PIN_motD_A 18 // Le codeur A du moteur Droit est sur la PIN 18 de l'arduino (interruption)
#define PIN_motD_B 19 // Le codeur B du moteur Droit est sur la PIN 19 de l'arduino (interruption)
#define PIN_motG_A 20 // Le codeur A du moteur Gauche est sur la PIN 20 de l'arduino (interruption)
#define PIN_motG_B 21 // Le codeur B du moteur Gauche est sur la PIN 21 de l'arduino (interruption)

#define ID_motD 1 // Identifiant du moteur droit
#define ID_motG 2 // Identifiant du moteur gauche
#define ID_mot_FunnyAction 3 // Identifiant moteur funny action

#define R_ROUE				4.45 	// Rayon des roues 
#define DCmotGEAR_RATIO		43.7 	// Rapport de réduction du moteur 
#define DCMOT_RES_VOIES 	64		// Résolution d'une voie
#define DCMOT_TENSION_ALIM	12		// Tension d'alimentation des moteurs CC de traction
#define DCMOT_CADENSE_MS	20		// Cadense de l'asservissement vitesse des moteurs tracteurs
#define DCMOT_MAX_REV		251		// Nombre de rotation par minutes maximum (donnée constructeur)

const double DCMOT_COUNTS_REV = DCmotGEAR_RATIO * DCMOT_RES_VOIES;	// Calcul du nombre de ticks par révolution en sortie du motoréducteur
const double DCMOT_CADENSE_S = DCMOT_CADENSE_MS / 1000.;	// Calucl de la cadense en secondes

#define MOT_CDA_STOP		0x00 	// Arreter le moteur
#define MOT_CDA_RUNFAST 	0x01 	// Avancer à vitesse max
#define MOT_CDA_RUNSMOOTH	0x02 	// Avancer à vitesse ralentie
#define MOT_CDA_INCREM		20		// Incrément pour la rampe de commande

#define DIR_STOP	 		0
#define DIR_AVANCE 			1
#define DIR_RECULE 			2
#define DIR_DROITE 			3
#define DIR_AVANCEDROITE 	4
#define DIR_RECULEDROITE 	5
#define DIR_GAUCHE 			6
#define DIR_AVANCEGAUCHE 	7
#define DIR_RECULEGAUCHE 	8

//** MACRO BLUETOOTH **//
#define BT_BAUDRATE 9600			// Vitesse de transmission de la liaison série avec le module bluetooth

#define BT_CODE_STOP			'0'
#define BT_CODE_AVANCE 			'1'
#define BT_CODE_DROITE 			'2'
#define BT_CODE_RECULE 			'3'
#define BT_CODE_GAUCHE 			'4'
#define BT_CODE_HAUTDROITE 		'5'
#define BT_CODE_BASDROITE 		'6'
#define BT_CODE_BASGAUCHE		'7'
#define BT_CODE_HAUTGAUCHE		'8'
#define BT_CODE_TPMSC			'a'
#define BT_CODE_TPGI			'b'
#define BT_CODE_TPMAT			'd'
#define BT_CODE_TPIOC			'c'

//** MACRO SERVOMOTEURS **//
#define PIN_SERVO_PIECE 10	// Servo moteur 

#define MS_ACTIONCOM 85000
#define MS_FUNNYACTION 90000
#define MS_TPSCOMPET 90000

#define PIN_KM1 23
#define PIN_KM2 24
#define PIN_KM3 25

#define PIN_LED_TP 31
#define PIN_LED_TIMER 33

#define vPWM_MAX 255
#define vPWM_TP 100

//** DECLARATION DES SERVOMOTEURS **//
Servo servoPieces;		// Servo moteur utilisé pour soulever les pièces au tp1
Servo servoBrasAvant;	// Servo moteur utilisé pour lever le bras avant

AF_DCMotor motrice_D(ID_motD); // Declaration du moteur de la roue droite
AF_DCMotor motrice_G(ID_motG); // Declaration du moteur de la roue gauche
AF_DCMotor mot_FunnyAction(3);

/***************************************
********** VARIABLES GLOBALES ********** 
***************************************/
volatile int motD_ticks=0, motG_ticks=0; 	// Compteurs de ticks
volatile double motD_vRad, motG_vRad; 		// Vitesse de rotation des moteurs en rad/s
volatile uint8_t motD_vPWM=0, motG_vPWM=0;	// Commande PWM des motrices
volatile uint8_t 	motD_dir=DIR_STOP, 
					motG_dir=DIR_STOP;	// Direction de chaque moteur
volatile uint8_t dir=DIR_STOP; // direction de commande du robot
volatile double diff_mot_vRad;
unsigned long ms_cycle, old_ms_cycle_affiche=0, old_ms_cycle_mot=0;
unsigned long currentTime;
bool startTimer = false;
unsigned long startTimerTime;
uint8_t vPWM=vPWM_MAX;
bool actionCom_isDone = false;
bool funnyAction_isDone = false;
bool endTp_isDone = false;

/**********************************************
********** DECLARATION DES FONCTIONS **********
**********************************************/
// !!! Problème avec la class HardwareSerial (pointeurs) -> Duplication de readInt() pour chaque port Serie
int PCreadInt(char endCarASCII = ASCII_LF){
	/** Variables utiles pour la lecture d'un entier dans le buffer serie
	* @param endCar	caractère indiquant la fin de la lecture (par défaut line feed)
	* @param cVar 	caractère reçu
	* @param iVar 	caractère reçu au format d'un entier
	* @param nombre valeur finale au format de chaine de caractere
	**/
	char cVar=0; int iVar=0;
	String nombre="";
	while(iVar != endCarASCII)
	{
		cVar = Serial.read();
		iVar = (int)cVar;
		if(iVar >= ASCII_0 && iVar <= ASCII_9){
			nombre += cVar; // Enregistrement des caracteres lues à la suite
											// afin de former le nombre sous forme 
											// de chaine de caracteres
			Serial.print(cVar);
		}
	}
	return nombre.toInt(); // Conversion de la chaine de caractere en nombre entier
}

void exec_TPGI(void){
	for(int i=0 ; i < 3 ; i++){
		servoPieces.write(90);
		delay(200);
		servoPieces.write(0);
		delay(1000);
	}
}


void motD_ticksA(void){
	// Le codeur A du moteur droit est détecté sur changement d'état
	if(digitalRead(PIN_motD_A) == digitalRead(PIN_motD_B)){
		// Le robot avance
		motD_ticks++;
	} else {
		// Le robot recule
		motD_ticks--;
	}
}

void motD_ticksB(void){
	// Le codeur B du moteur droit est détecté sur changement d'état
	if(digitalRead(PIN_motD_A) == digitalRead(PIN_motD_B)){
		// Le robot recule
		motD_ticks--;
	} else {
		// Le robot avance
		motD_ticks++;
	}
}

void motG_ticksA(void){
	// Le codeur A du moteur droit est détecté sur changement d'état
	if(digitalRead(PIN_motG_A) == digitalRead(PIN_motG_B)){
		// Le robot avance
		motG_ticks++;
	} else {
		// Le robot recule
		motG_ticks--;
	}
}

void motG_ticksB(void){
	// Le codeur B du moteur droit est détecté sur changement d'état
	if(digitalRead(PIN_motG_A) == digitalRead(PIN_motG_B)){
		// Le robot recule
		motG_ticks--;
	} else {
		// Le robot avance
		motG_ticks++;
	}
}

void ctrl_vMot(void){
	// contrôle de la vitesse des moteurs

	static volatile unsigned int motD_dticks, motG_dticks;	// Nombre de ticks depuis la dernière lecture

	motD_dticks = motD_ticks;
	motG_dticks = motG_ticks;

	motD_ticks = motG_ticks = 0; // Remise à zéro pour calculer delta ticks pour vitesse

	// Calcul de la vitesse de rotation. C'est le nombre d'impulsions converti en radian, divisé par la période d'échantillonnage
	motD_vRad = ((2.* PI * ((double)motD_dticks)) / DCMOT_COUNTS_REV) / DCMOT_CADENSE_S; // en rad/s
	motG_vRad = -((2.* PI * ((double)motG_dticks)) / DCMOT_COUNTS_REV) / DCMOT_CADENSE_S; // en rad/s

	switch (dir)
	{
	case DIR_STOP:
		motrice_D.run(BRAKE);
		motD_vPWM=0;
		motrice_G.run(BRAKE);
		motG_vPWM=0;
		break;

	case DIR_AVANCE:
		motrice_D.run(FORWARD);
		if(motD_vPWM < 30)motD_vPWM = 30;
		else if(motD_vPWM + 32 < vPWM) motD_vPWM += 32;
		else motD_vPWM = vPWM;

		motrice_G.run(FORWARD);
		if(motG_vPWM < 25)motG_vPWM = 25;
		else if(motG_vPWM + 20 < vPWM-15) motG_vPWM += 20;
		else motG_vPWM = vPWM-15;
		break;

	case DIR_RECULE:
		motrice_D.run(BACKWARD);
		if(motD_vPWM < 30)motD_vPWM = 30;
		else if(motD_vPWM + 32 < vPWM) motD_vPWM += 32;
		else motD_vPWM = vPWM;

		motrice_G.run(BACKWARD);
		if(motG_vPWM < 25)motG_vPWM = 25;
		else if(motG_vPWM + 20 < vPWM-15) motG_vPWM += 20;
		else motG_vPWM = vPWM-15;
		break;
	
	case DIR_DROITE:
		motrice_D.run(BACKWARD);
		if(motD_vPWM < 25)motD_vPWM = 25;
		else if(motD_vPWM + 20 < vPWM) motD_vPWM += 20;
		else motD_vPWM = vPWM;

		motrice_G.run(FORWARD);
		if(motG_vPWM < 25)motG_vPWM = 25;
		else if(motG_vPWM + 20 < vPWM) motG_vPWM += 20;
		else motG_vPWM = vPWM;
		break;

	case DIR_GAUCHE:
		motrice_D.run(FORWARD);
		if(motD_vPWM < 25)motD_vPWM = 27;
		else if(motD_vPWM + 20 < vPWM) motD_vPWM += 20;
		else motD_vPWM = vPWM;

		motrice_G.run(BACKWARD);
		if(motG_vPWM < 25)motG_vPWM = 25;
		else if(motG_vPWM + 20 < vPWM) motG_vPWM += 20;
		else motG_vPWM = vPWM;
		break;
	}

	if(!startTimer && dir != DIR_STOP) {
		startTimer = true;
		startTimerTime = millis();
		digitalWrite(PIN_LED_TIMER, HIGH);
	}
	
	// Serial.print(vPWM);
	// Serial.print(" , ");
	// Serial.print(motD_vPWM);
	// Serial.print(" , ");
	// Serial.println(motG_vPWM);
	motrice_D.setSpeed(motD_vPWM);
	motrice_G.setSpeed(motG_vPWM);
}

void exec_TPMAT(){
	digitalWrite(PIN_KM2, LOW); // On active le relais du ventilateur branchÈ sur le pin 2 sur 1 
	vPWM = vPWM_TP;
	dir=DIR_STOP;
	dir=DIR_RECULE;
	delay(1300); // pendant une durÈe de 5 secondes
	dir = DIR_GAUCHE;
	delay(400);
	dir = DIR_STOP;
	delay(1000);
	for(int i=0 ; i<6 ; i++){
		dir=DIR_RECULE;
		delay(400);
		dir=DIR_STOP;
		delay(500);
		dir = DIR_GAUCHE;
		delay(300);
	}
	dir=DIR_STOP;
	vPWM=vPWM_MAX;
	digitalWrite(PIN_KM2, HIGH); // on dÈsactive le relais du ventilatuer branchÈ sur le pin 2 sur 0
}

void exec_FunnyAction(void){
	dir = DIR_STOP;
	mot_FunnyAction.run(BACKWARD);
	mot_FunnyAction.setSpeed(255);
	delay(4300);
	mot_FunnyAction.setSpeed(0);
	delay(1000);
	mot_FunnyAction.run(RELEASE);
}

void exec_TPIOC(void) {
	unsigned int lum = analogRead(9);// capteur de luminositÈ branchÈ sur le pin 8 en analogique
	unsigned long initTps;
	dir = DIR_AVANCE;// le robot avance
	initTps = millis();
	while(millis() < initTps + 3000 && lum < 900){
		lum = analogRead(9);
		Serial.println(lum);
	}

	dir = DIR_STOP;

	if (lum > 900){ // si le capteur de luminositÈ renvoie un signal analogique supÈrieur ‡ 900
		dir = DIR_AVANCE; // le robot avance
		while(lum > 900)lum = analogRead(9); // tant que le capteur de luminositÈ renvoie un signal analogique supÈrieur ‡ 900
		dir = DIR_RECULE; // le robot recule
		delay(2000);
		dir = DIR_STOP;
	}
	else {
		dir = DIR_RECULE;
		delay(3000);
		dir = DIR_DROITE;
		delay(3000);
		dir = DIR_AVANCE;
		while(lum > 900){
			lum = analogRead(9);
			Serial.println(lum);
		}
		dir = DIR_RECULE;
		delay(3000); 
		dir = DIR_STOP;
	}
}

void exec_TPMSC(void){
	servoBrasAvant.write(95);
	dir = DIR_AVANCE;
	delay(1300);
	dir = DIR_RECULE;
	delay(1200);
	dir = DIR_STOP;

	servoBrasAvant.write(180);
	digitalWrite(PIN_KM1, LOW);

	dir = DIR_AVANCE;
	delay(1300);
	dir = DIR_STOP;
	delay(1000);
	servoBrasAvant.write(95);
	delay(500);
	digitalWrite(PIN_KM1, HIGH);
	dir = DIR_STOP;
}

/*****************************************
********** TRAITEMENT PRINCIPAL **********
******************************************/

void setup() {
	Serial.begin(9600); // Activation liaison série
	Serial3.begin(9600);

	// init encodeur moteurs dc
	pinMode(PIN_motD_A, INPUT);
	pinMode(PIN_motD_B, INPUT);
	pinMode(PIN_motG_A, INPUT);
	pinMode(PIN_motG_B, INPUT);

	pinMode(PIN_KM1, OUTPUT);
	pinMode(PIN_KM2, OUTPUT);
	pinMode(PIN_KM3, OUTPUT);
	digitalWrite(PIN_KM1, HIGH);
	digitalWrite(PIN_KM2, HIGH);
	digitalWrite(PIN_KM3, HIGH);

	pinMode(PIN_LED_TIMER, OUTPUT);
	pinMode(PIN_LED_TP, OUTPUT);
	digitalWrite(PIN_LED_TIMER, LOW);
	digitalWrite(PIN_LED_TP, LOW);

	// Pin sur Interruptions
	// attachInterrupt(digitalPinToInterrupt(PIN_motD_A), motD_ticksA, CHANGE);
	// attachInterrupt(digitalPinToInterrupt(PIN_motD_B), motD_ticksB, CHANGE);
	// attachInterrupt(digitalPinToInterrupt(PIN_motG_A), motG_ticksA, CHANGE);
	// attachInterrupt(digitalPinToInterrupt(PIN_motG_B), motG_ticksB, CHANGE);

	FlexiTimer2::set(DCMOT_CADENSE_MS, ctrl_vMot); // MsTimer2 style is also supported
  	FlexiTimer2::start();


	servoBrasAvant.attach(9);
	servoBrasAvant.write(95);
	// Déclaration des servo moteurs
	servoPieces.attach(PIN_SERVO_PIECE);
	servoPieces.write(0);
}


unsigned long t0=0;
void loop() {
	if (Serial.available() > 0){
		// DEBUG : Commande depuis le moniteur série
		char c = Serial.read();
		if(c == 'z') dir = DIR_AVANCE;
		else if(c == 'x') dir = DIR_RECULE;
		else if(c == 'd') dir = DIR_DROITE;
		else if(c == 'q') dir = DIR_GAUCHE;
		else if(c=='s') dir = DIR_STOP;
		else if(c=='t') servoBrasAvant.write(180);
		else if(c=='T') servoBrasAvant.write(95);
		else if(c=='y') digitalWrite(PIN_KM1, LOW);
		else if(c=='Y') digitalWrite(PIN_KM1, HIGH);
		else if(c=='v') digitalWrite(PIN_KM2, LOW);
		else if(c=='V') digitalWrite(PIN_KM2, HIGH);
	} 

	if(Serial3.available() > 0){
		char c = Serial3.read();
		switch (c)
		{
		case BT_CODE_STOP:
			dir=DIR_STOP;
			break;
		case BT_CODE_AVANCE:
			dir=DIR_AVANCE;
			break;
		case BT_CODE_RECULE:
			dir=DIR_RECULE;
			break;
		case BT_CODE_DROITE:
			dir=DIR_DROITE;
			break;
		case BT_CODE_GAUCHE:
			dir=DIR_GAUCHE;
			break;
		
		case BT_CODE_TPMSC:
			digitalWrite(PIN_LED_TP, HIGH);
			exec_TPMSC();
			digitalWrite(PIN_LED_TP, LOW);
			break;
		case BT_CODE_TPGI:
			digitalWrite(PIN_LED_TP, HIGH);
			exec_TPGI();
			digitalWrite(PIN_LED_TP, LOW);
			break;
		case BT_CODE_TPMAT:
			digitalWrite(PIN_LED_TP, HIGH);
			exec_TPMAT();
			digitalWrite(PIN_LED_TP, LOW);
			break;
		case BT_CODE_TPIOC:
			digitalWrite(PIN_LED_TP, HIGH);
			exec_TPIOC();
			digitalWrite(PIN_LED_TP, LOW);
			break;
		}
		
	}

	if(startTimer){
		currentTime = millis();

		if(!actionCom_isDone && currentTime > startTimerTime + MS_ACTIONCOM){
			dir = DIR_STOP;
			digitalWrite(PIN_KM3, LOW);
			actionCom_isDone=true;
		}
		else if(!funnyAction_isDone && currentTime > startTimerTime + MS_FUNNYACTION) {
			exec_FunnyAction();
			funnyAction_isDone=true;
		}
		else if(!endTp_isDone && currentTime > startTimerTime + MS_TPSCOMPET){
			digitalWrite(PIN_LED_TIMER, LOW);
			endTp_isDone=true;
		}
	}

	if(millis() > t0 + 500){

		t0 = millis();
	}
}